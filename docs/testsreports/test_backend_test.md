# Test sur backend de test

!!! check "TEST OK"


Pour tester le bon fonctionnement du firmware le test suivant a été réalisé via les script **.c** de nan135.

Lancement de l'acquisition SGDMA : 
```
./radiogaga 24 < dma_bus_occupation_test.txt 
```
Des fichiers sous ce format sortent chaque seconde:
??? info "dma_message_0001.txt"

    ```hex title="dma_message_0001.txt"
    0x00181818
    0xffffffff
    0x05f3ffc7
    0x05f2385c
    0x05e6973c
    0x05d0c9e8
    0x05a61c77
    0x0555a91b
    0x04ade4a2
    0x0293031b
    0x00096250
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3eaa0
    0x05ebe3d5
    0x05d560e1
    0x05aa21bd
    0x05544c03
    0x049bcad5
    0x03677caf
    0x002cd7ae
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3ffc7
    0x05f2385c
    0x05e6973c
    0x05d0c9e8
    0x05a61c77
    0x0555a91b
    0x04ade4a2
    0x0293031b
    0x00096250
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3eaa0
    0x05ebe3d5
    0x05d560e1
    0x05aa21bd
    0x05544c03
    0x049bcad5
    0x03677caf
    0x002cd7ae
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3ffc7
    0x05f2385c
    0x05e6973c
    0x05d0c9e8
    0x05a61c77
    0x0555a91b
    0x04ade4a2
    0x0293031b
    0x00096250
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3eaa0
    0x05ebe3d5
    0x05d560e1
    0x05aa21bd
    0x05544c03
    0x049bcad5
    0x03677caf
    0x002cd7ae
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3ffc7
    0x05f2385c
    0x05e6973c
    0x05d0c9e8
    0x05a61c77
    0x0555a91b
    0x04ade4a2
    0x0293031b
    0x00096250
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x05f3eaa0
    0x05ebe3d5
    0x05d560e1
    0x05aa21bd
    0x05544c03
    0x049bcad5
    0x03677caf
    0x002cd7ae
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0x00000000
    0xa75e0b5e
    ```

Ensuite on peut les lire et plotter des histograms avec les script suivant : 

??? info "test_dma.py"

    ``` py title="test_dma.py"

    #!/usr/bin/python
    # -*- coding: utf-8 -*-

    import glob
    import matplotlib.pyplot as plt
    import matplotlib.dates as md
    import numpy as np
    import datetime as dt

    file = sorted(glob.glob("dma_message_*.txt"))
    print(file)
    file = file[-1]
    col_names   = ('histogram')
    col_formats = ( 'i4' )

    N_word = 112
    dat = np.loadtxt(file, skiprows=2, max_rows=N_word, dtype=('i4'), converters={_:lambda s: int(s, 16) for _ in range(1)})

    print(dat.shape)
    print(dat)
    dat = dat.reshape(-1,14)

    print(dat.shape)
    print(dat)
    print('\n\n')
    print(dat[0,:])




    fig,axs = plt.subplots(4,2)
    print(len(axs))
    for i in range(4):
      ## X
      axs[i,0].bar(np.arange(0,14),dat[i*2,:])
      axs[i,0].set_title  (f'ADC {i} X')
      axs[i,0].set_yscale('log')
      ## Y
      axs[i,1].bar(np.arange(0,14),dat[i*2+1,:])
      axs[i,1].set_title  (f'ADC {i} Y')
      axs[i,1].set_yscale('log')
    plt.show()

    ```
Ce qui sort le plot suivant : 

![Test sur 1 carte du backend de test](../figures/test_ok.png)