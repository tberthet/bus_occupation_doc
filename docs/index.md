# BUS_OCCUPATION

Le module BUS_OCCUPATION est un module de statistiques permettant d'analyser l'occupation de différents bus de données au sein du firmware dans le FPGA.
Le but étant d'analyser si les bus sont sous utilisés ou à l'inverser voir certains overflows. 
L'algo du module est tout simple, le module compte le nombre de fois que chaque bit apparaît pendant 1 sec. 
Par exemple sur un bruit gaussien avec une std de 100 et un overflow, durant 1 seconde sur un bus de 14 bits voilà le résultat : 

![Simus python](figures/bus_occupation_simu.png)

Le but étant d'itérer ce processus sur : 

- Les 8 voies ADC (8 * 14 bits)  
- Le bus SDO (2 * 4 * 18 bits)  
- Les 4 beams ( 4 * 24 bits)    
  
Pour chaque bit l'accumulation est codé sur un mot de 32 bits lisible dans l'interface registre. 
Ce qui représente en tout 352 mots de 32 bits à lire par carte chaque seconde. 

