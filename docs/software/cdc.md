# Cahiers des charges

## Mappage registre et SGDMA
Chaque seconde sur chacune des cartes il faut récupérer l'histogramme de chacun des bus.  
Pour cela il faut passer par le module monitor. La config SGDMA est la suivante :   


!!! info "Syntax de la config SGDMA"

    ```
    # ADC 0   
    AW 00840003 0000100E # lecture de 14 mots de 32 bits par pas de 1 adresse
    AW 00840003 01400000 # à partir de cette adresse
    ```

??? note "dma_bus_occupation.txt"
    ``` txt title="dma_bus_occupation.txt"
    #############################################################################

    # Generates Avalon-MM Master Accesses (REMOTE MASTER REGISTERS)
    # Syntax is (ADDR and DATA are 32bits hexa)
    # Write : AW ADDR DATA
    # Read  : AR ADDR

    M DMA_CTRL : OFF
    AW 00840000 00000000

    M TX_DELAY
    AW 00840001 00001000

    M RAM Descriptors Address
    AW 00840002 00000000


    # BUS_OCCUPATION statistics

    # ADC 0 
    AW 00840003 0000100E
    AW 00840003 01400000

    # ADC 1 
    AW 00840003 0000100E
    AW 00840003 01400020

    # ADC 2 
    AW 00840003 0000100E
    AW 00840003 01400040

    # ADC 3 
    AW 00840003 0000100E
    AW 00840003 01400060

    # ADC 4 
    AW 00840003 0000100E
    AW 00840003 01400080

    # ADC 5 
    AW 00840003 0000100E
    AW 00840003 014000A0

    # ADC 6 
    AW 00840003 0000100E
    AW 00840003 014000C0

    # ADC 7 
    AW 00840003 0000100E
    AW 00840003 014000E0

    # SDO 0 
    AW 00840003 0000100E
    AW 00840003 01400100

    # SDO 1 
    AW 00840003 0000100E
    AW 00840003 01400120

    # SDO 2 
    AW 00840003 0000100E
    AW 00840003 01400140

    # SDO 3 
    AW 00840003 0000100E
    AW 00840003 01400160

    # SDO 4 
    AW 00840003 0000100E
    AW 00840003 01400180

    # SDO 5 
    AW 00840003 0000100E
    AW 00840003 014001A0

    # SDO 6 
    AW 00840003 0000100E
    AW 00840003 014001C0

    # SDO 7 
    AW 00840003 0000100E
    AW 00840003 014001E0

    # BEAM 0 
    AW 00840003 0000100E
    AW 00840003 01400200

    # BEAM 1 
    AW 00840003 0000100E
    AW 00840003 01400220

    # BEAM 2 
    AW 00840003 0000100E
    AW 00840003 01400240

    # BEAM 3 
    AW 00840003 0000100E
    AW 00840003 01400260




    # descripteur de fin
    AW 00840003 80000000

    #			DMA_CTRL : RUN
    AW 00840000 00000001

    #		 DMA_CTRL : STOP
    #AW 00840000 00000000

    #############################################################################

    # Enable internal PPS
    AW 00000007 00000001


    # descripteur de fin
    AW 00840003 80000000

    #			DMA_CTRL : RUN
    AW 00840000 00000001

    #		 DMA_CTRL : STOP
    #AW 00840000 00000000

    #############################################################################

    # Enable internal PPS
    AW 00000007 00000001
    ```

Et voici le mappage mémoire des différents registres : 
??? note "RSP_constants.h"

    ```c
    #define c_BUS_OCCUPATION_n_bits_adc         (        14)  //  ####  Computed from: 14
    #define c_BUS_OCCUPATION_n_bits_sdo         (        18)  //  ####  Computed from: 18
    #define c_BUS_OCCUPATION_n_bits_beam        (        24)  //  ####  Computed from: 24


    #define c_BUS_OCCUPATION_n_chan_adc         (         4)  //  ####  Computed from: 4
    #define c_BUS_OCCUPATION_n_chan_sdo         (         8)  //  ####  Computed from: 2 * 4
    #define c_BUS_OCCUPATION_n_chan_beam        (         4)  //  ####  Computed from: 4

    #define c_BUS_OCCUPATION_address_offset     (  20971520)  //  ####  Computed from: 16#0140_0000#
    #define C_BUS_OCCUPATION_histogram_ADC_x_arr_0 (         0)  //  ####  Computed from: 16#0#
    #define C_BUS_OCCUPATION_histogram_ADC_y_arr_0 (        32)  //  ####  Computed from: 16#20#
    #define C_BUS_OCCUPATION_histogram_ADC_x_arr_1 (        64)  //  ####  Computed from: 16#40#
    #define C_BUS_OCCUPATION_histogram_ADC_y_arr_1 (        96)  //  ####  Computed from: 16#60#
    #define C_BUS_OCCUPATION_histogram_ADC_x_arr_2 (       128)  //  ####  Computed from: 16#80#
    #define C_BUS_OCCUPATION_histogram_ADC_y_arr_2 (       160)  //  ####  Computed from: 16#A0#
    #define C_BUS_OCCUPATION_histogram_ADC_x_arr_3 (       192)  //  ####  Computed from: 16#C0#
    #define C_BUS_OCCUPATION_histogram_ADC_y_arr_3 (       224)  //  ####  Computed from: 16#E0#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_0 (       256)  //  ####  Computed from: 16#100#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_1 (       288)  //  ####  Computed from: 16#120#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_2 (       320)  //  ####  Computed from: 16#140#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_3 (       352)  //  ####  Computed from: 16#160#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_4 (       384)  //  ####  Computed from: 16#180#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_5 (       416)  //  ####  Computed from: 16#1A0#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_6 (       448)  //  ####  Computed from: 16#1C0#
    #define C_BUS_OCCUPATION_histogram_SDO_arr_7 (       480)  //  ####  Computed from: 16#1E0#
    #define C_BUS_OCCUPATION_histogram_beam_arr_0 (       512)  //  ####  Computed from: 16#200#
    #define C_BUS_OCCUPATION_histogram_beam_arr_1 (       544)  //  ####  Computed from: 16#220#
    #define C_BUS_OCCUPATION_histogram_beam_arr_2 (       576)  //  ####  Computed from: 16#240#
    #define C_BUS_OCCUPATION_histogram_beam_arr_3 (       608)  //  ####  Computed from: 16#260#
    ```

Soit pour chaque ligne correspondant à l'histogramme d'un **bus**, on va devoir lire chacun des mots correspondant à chacun des **bits** du bus.  
Par exemple pour l'**ADC_Y_0** on va lire 14 mots de 32 bits de l'adresse :  
**16#140_00200#** à  **16#140_0020E#**

## Subtilité BEAM

Les bus nommés **beam** sont les signaux sortant de RING_ADDER. Donc seuls les statistiques de la dernière carte seront cohérent.

## Affichage graphana

Pour la visualition dans graphana ce serait un genre d'histogramme en fonction du temps comme ceci :

![Heatmap graphana](../figures/graphana.png)