# BUS_OCCUPATION

Ensuite le module histogram est instancié pour chaque bus avec des generates soit :  

- **ADC** : 8 * 14 bits 
- **SDO** : 8  * 18 bits
- **beam** : 4 * 24 bits

Ensuite les sorties de chaques histogrames sont connectés dans l'interface registre de cette manière suivante.
L'adresse est codé sur 10 bits. Les 5 bits de poids forts permettent de selectionner le bus et les 5 de poids faibles les bits.  
Soit 32 bus de 32 bits possibles.
``` vhdl
process(avs_clk, avs_rst)
    variable addr_nibble_low  : integer range 0 to 31;
    variable addr_nibble_high : integer range 0 to 31;
  begin
    if avs_rst = '1' then
      avs_rddav         <= '0';
      avs_rddata        <= (others => '0');
    elsif rising_edge(avs_clk) then
      addr_nibble_low  := to_integer(unsigned(avs_addr(4 downto 0)));
      addr_nibble_high := to_integer(unsigned(avs_addr(9 downto 5)));
      --
      avs_rddata <= (others => '0') ;
      avs_rddav  <= '0';

        -- READ Requests
        if avs_rdreq = '1' then
          avs_rddav  <= '1';
          case addr_nibble_high is
            when 16#0#   => avs_rddata      <= histogram_ADC_x_arr_r(0)(addr_nibble_low); 
            when 16#1#   => avs_rddata      <= histogram_ADC_y_arr_r(0)(addr_nibble_low); 
            when 16#2#   => avs_rddata      <= histogram_ADC_x_arr_r(1)(addr_nibble_low); 
            when 16#3#   => avs_rddata      <= histogram_ADC_y_arr_r(1)(addr_nibble_low); 
            when 16#4#   => avs_rddata      <= histogram_ADC_x_arr_r(2)(addr_nibble_low); 
            when 16#5#   => avs_rddata      <= histogram_ADC_y_arr_r(2)(addr_nibble_low); 
            when 16#6#   => avs_rddata      <= histogram_ADC_x_arr_r(3)(addr_nibble_low); 
            when 16#7#   => avs_rddata      <= histogram_ADC_y_arr_r(3)(addr_nibble_low); 
            when 16#8#   => avs_rddata      <= histogram_SDO_arr_r(0)(addr_nibble_low); 
            when 16#9#   => avs_rddata      <= histogram_SDO_arr_r(1)(addr_nibble_low); 
            when 16#A#   => avs_rddata      <= histogram_SDO_arr_r(2)(addr_nibble_low); 
            when 16#B#   => avs_rddata      <= histogram_SDO_arr_r(3)(addr_nibble_low); 
            when 16#C#   => avs_rddata      <= histogram_SDO_arr_r(4)(addr_nibble_low); 
            when 16#D#   => avs_rddata      <= histogram_SDO_arr_r(5)(addr_nibble_low); 
            when 16#E#   => avs_rddata      <= histogram_SDO_arr_r(6)(addr_nibble_low); 
            when 16#F#   => avs_rddata      <= histogram_SDO_arr_r(7)(addr_nibble_low);
            when 16#10#  => avs_rddata      <= histogram_beam_arr_r(0)(addr_nibble_low); 
            when 16#11#  => avs_rddata      <= histogram_beam_arr_r(1)(addr_nibble_low); 
            when 16#12#  => avs_rddata      <= histogram_beam_arr_r(2)(addr_nibble_low); 
            when 16#13#  => avs_rddata      <= histogram_beam_arr_r(3)(addr_nibble_low); 

            when others => null;
          end case;
        end if;

    end if;
  end process;
```

