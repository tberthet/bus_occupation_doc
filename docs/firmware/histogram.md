# histogram

Le module permettant de réaliser les statistiques est relativement simple, il s'agit juste d'un compteur comptant le nombre de fois qu'un bit est **'1'** sur chacun des bits du bus durant 1 seconde.

Le module vhdl se trouve dans PROCESSING_MODULES/histogram/

``` vhdl

entity histogram
 is
  generic (
    g_data_w       : natural
  );
  port (
    -- Data Path    
    dp_rst : in std_logic;
    dp_clk : in std_logic;

    sync : in std_logic;

    data_in : in std_logic_vector(g_data_w - 1 downto 0);
    DAV     : in std_logic;


    histogram_out : out hist_arr
    
  );
end entity;


architecture rtl of histogram
 is
  signal data_in_abs     : std_logic_vector(g_data_w - 1 downto 0);
  signal histogram_acc : hist_arr;
  signal histogram_out_r : hist_arr;

begin
  data_in_abs <=std_logic_vector( abs(signed(data_in)));
  gen_hist : for idx in 0 to g_data_w - 1 generate
    
    histogram_acc(idx) <=  (others => '0')  when sync= '1' and rising_edge(dp_clk) 
                           else std_logic_vector(unsigned(histogram_acc(idx)) + 1 ) when data_in_abs(idx) = '1' and DAV = '1'  and rising_edge(dp_clk)  ;

    histogram_out_r(idx) <= histogram_acc(idx) when sync= '1' and rising_edge(dp_clk); 
  end generate;
  
    histogram_out <= histogram_out_r;

end architecture;


```
